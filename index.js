const cheerio = require('cheerio');
const request = require('request');
const utf8 = require('utf8');

const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

async function asyncForEach(array, plus, callback) {
    for (let index = 0; index < array.length; index += plus) {
        await callback(array[index], index, array)
    }
}
test = [];

function hex2utf8(d) {
    var b = 0;
    var a = "";
    while (b < d.length) {
        c = parseInt(d[b], 16) & 255;
        if (c < 128) {
            if (c < 16) {
                switch (c) {
                    case 9:
                        a += " ";
                        break;
                    case 13:
                        a += "\r";
                        break;
                    case 10:
                        a += "\n";
                        break
                }
            } else {
                a += String.fromCharCode(c)
            }
            b++
        } else {
            if ((c > 191) && (c < 224)) {
                if (b + 1 < d.length) {
                    c2 = parseInt(d[b + 1], 16) & 255;
                    a += String.fromCharCode(((c & 31) << 6) | (c2 & 63))
                }
                b += 2
            } else {
                if (b + 2 < d.length) {
                    c2 = parseInt(d[b + 1], 16) & 255;
                    c3 = parseInt(d[b + 2], 16) & 255;
                    a += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63))
                }
                b += 3
            }
        }
    }
    return a
}

function DS(e, g) {
    var b = [];
    b["~"] = "0";
    b.H = "1";
    b["^"] = "2";
    b["@"] = "3";
    b.f = "4";
    b["0"] = "5";
    b["5"] = "6";
    b.n = "7";
    b.r = "8";
    b["="] = "9";
    b.W = "a";
    b.L = "b";
    b["7"] = "c";
    b[" "] = "d";
    b.u = "e";
    b.c = "f";
    var f = [];
    var d = 0;
    for (var a = 0; a < g.length; a += 2) {
        f[d++] = b[g.substring(a, a + 1)] + b[g.substring(a + 1, a + 2)]
    }
    return hex2utf8(f)
}

async function decode(data) {
    let ao = data.split(/\n/);
    let container = [];
    // console.log(ao);
    await asyncForEach(ao, 1, async function (S, index) {
        // console.log(S);
        let decodedstr = await DS("1978thepasswhere", ao[index]);
        decodedstr = decodedstr.replace(/\r\n/g, "  ");
        // console.log(decodedstr);
        container.push(decodedstr + "\n");
    });
    console.log(container.join(' '));
    // console.log(container);
    // console.log(container.join('1'));
    // console.log(utf8.decode(test.join('')));

    // for (let S = 0; S < ao.length; S++) {
    //
    // }
}

function start() {
    request({url: 'https://zelluloza.ru/books/6870/53513/'}, function (error, response, body) {
        // console.log(body);
        let cheers = cheerio.load(body);
        let script = cheers('script').get()[5]['children'][0].data.trim();
        let book = script.split('\n')[3].trim().replace(/(ajax\()|(\);)|'|\s/g, '').split(',');
        request.post({
            headers: {'content-type': 'application/x-www-form-urlencoded'},
            url: 'https://zelluloza.ru/',
            form: {
                op: 'getbook',
                par1: book[3],
                par2: book[4],
                par4: book[5]
            }
        }, async function (error, response, body) {
            // console.log(response);
            await decode(body);
        });
    });


}

start();